const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './app/index.html',
    filename: 'index.html',
    inject: 'body'
});

const config = {
    entry: './app/index.js',
    output:{
        path: path.resolve('dist'),
        filename: 'bundle.js'
    },
    module:{
        rules: [
            {
                test: /.js$/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test:   /\.(s*)css$/,
                use: ['style-loader', 'css-loader', 'sass-loader'] 
            },
        ]

    },
    plugins: [HtmlWebpackPluginConfig]
};

module.exports= config;