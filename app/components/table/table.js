import React from 'react';

export default class Table extends React.Component {
    
    render() {
        // Constantes & variables
        const digits = ["7","8","9","4","5","6","1","2","3"]
        const btnDigits= digits.map((dgt) => 
            <div className="border border-secundary col-4 d-flex justify-content-center p-4" onClick={ () => {this.props.digit(dgt)}}>{dgt}</div>
        );
        const classOp1="border border-warning col-12 d-flex justify-content-center p-4"
        const classOp2="border border-warning col-3 d-flex justify-content-center p-4"
       
        // JSX
        return (
            <div className="mytable">
                <div className="row">
                    <div className="col-9">
                        <div className="row">
                            {btnDigits}
                            <div className="border border-secundary col-12 d-flex justify-content-center p-4" onClick={ () => {this.props.digit("0")}}>0</div>
                        </div>
                    </div>
                    <div className="col-3">
                        <div className="row">
                            <div className={classOp1} onClick={ () => {this.props.oper("+")}}>+</div>
                            <div className={classOp1} onClick={ () => {this.props.oper("-")}}>-</div>
                            <div className={classOp1} onClick={ () => {this.props.oper("*")}}>x</div>
                            <div className={classOp1} onClick={ () => {this.props.oper("/")}}>/</div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className={classOp2} onClick={ () => {this.props.decimal()}}>.</div>
                    <div className={classOp2} onClick={ () => {this.props.clearOp()}}>C</div>
                    <div className={classOp2} onClick={ () => {this.props.clearDigit()}}><i class="fas fa-arrow-left"></i></div>        
                    <div className={classOp2} onClick={ () => {this.props.oper("")}}>=</div>
                </div>

            </div>
        )
    };

}