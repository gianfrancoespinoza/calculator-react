import Operation from './operation/operation';
import Table from './table/table';
import React from 'react';


export default class App extends React.Component {
    constructor(){
        super();
        this.state = {
            history: [],
            result: "0",
            total: "0",
            decim: false,
            psing: "",
            end:false
        }
    this.state.digit=this.digit.bind(this)   
    this.state.clearOp=this.clearOp.bind(this) 
    this.state.clearDigit=this.clearDigit.bind(this)     
    this.state.decimal=this.decimal.bind(this) 
    // this.state.sumOp=this.sumOp.bind(this) 
    // this.state.resOp=this.resOp.bind(this)
    // this.state.resultOp=this.resultOp.bind(this) 
    this.state.oper=this.oper.bind(this)
    }

    oper(e) {
        if (e == "*" || e == "/"){
            this.setState(function(prevState){ 
                if (parseFloat(prevState.result) == 0) {
                    return{
                        psing: e, 
                        total: eval( parseString(prevState.total) + prevState.psing + parseString(prevState.result)),
                        decim: false
                    }
                }
                else {
                    return{
                        psing: e, 
                        total: eval( parseString(prevState.total) + prevState.psing + parseString(prevState.result)),
                        history: [...prevState.history, prevState.psing + prevState.result],
                        decim: false
                    }    
                }
            })
        }
        else {
            this.setState(function(prevState){ 
                if (parseFloat(prevState.result) == 0) {
                    return{
                        psing: e, 
                        total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
                        result: "0",
                        decim: false
                    }
                }
                else {
                    return{
                        psing: e, 
                        total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
                        history: [...prevState.history, prevState.psing + Intl.NumberFormat("de-DE").format(prevState.result)],
                        result: "0",
                        decim: false
                    }    
                }
            })
        }
    }




    // resultOp() {
    //     this.setState(function(prevState){ 
    //         if (parseFloat(prevState.result) == 0) {
    //             return{
    //                 psing: "", 
    //                 total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
    //                 result: "0",
    //                 decim: false
    //             }
    //         }
    //         else {
    //             return{
    //                 psing: "", 
    //                 total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
    //                 history: [...prevState.history, prevState.psing + prevState.result],
    //                 result: "0",
    //                 decim: false
    //             }    
    //         }
    //     })

    // }

    // sumOp() {
    //     this.setState(function(prevState){
    //         return{
    //             psing: "+", 
    //             total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
    //             history: [...prevState.history, prevState.psing + prevState.result],
    //             result: "0",
    //             decim: false
    //         }
    //     })

    // }

    // resOp() {
    //     this.setState(function(prevState){
    //         return{
    //             psing: "-",
    //             total: parseFloat(prevState.total) + parseFloat(prevState.psing + prevState.result),
    //             history: [...prevState.history, prevState.psing + prevState.result],
    //             result: "0",
    //             decim: false
    //         }
    //     })

    // }

    decimal() {
        this.setState(function(prevState){
            if (prevState.decim == false){
                return{
                    result: prevState.result + ".",
                    decim: true
                }
            }
        })
        
    }
    
    clearOp() {
        this.setState({result: "0"})
        this.setState({total: "0"})
        this.setState({psing: ""})
        this.setState({decim: false})
        this.setState({history: []})

    }

    clearDigit() {
        this.setState(function(prevState){
            if(prevState.result.length == 1){
                this.setState({result: "0"})
                this.setState({decim: false})
            }
            else if(prevState.result != "0"){
                return {
                    result: prevState.result.replace(/.$/,'')
                }
            }
        })
    }
    

    digit(e) {
        this.setState(function(prevState){
            if(prevState.result == "0"){
                this.setState({result: e})
            }
            else{
                return {
                    result: prevState.result + e
                }    
            }
        })
    }

    render () {
        return (
               <div className="max-screen">
                    <Operation  total={this.state.total} psing={this.state.psing} result={this.state.result} history={this.state.history}/>
                    <Table oper={this.state.oper} resultOp={this.state.resultOp} resOp={this.state.resOp} sumOp={this.state.sumOp} decimal={this.state.decimal} clearDigit = {this.state.clearDigit} digit = {this.state.digit} clearOp = {this.state.clearOp}/>
                </div>
        )
    }

}
