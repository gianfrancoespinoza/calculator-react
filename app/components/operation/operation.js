import React from 'react';

export default class Operation extends React.Component {
    constructor(props){
        super(props)
    }
    
    render() {
        if (parseFloat(this.props.result) == 0){
            return (
                <div className="operator row pr-0 pl-0">
                    <div className="col-12 d-flex justify-content-end">{this.props.history}</div>
                    <div className="col-12 d-flex justify-content-end">{Intl.NumberFormat("de-DE").format(this.props.total)}</div>
                </div>  
            )
        } 
        else{
            return (
                <div className="operator row pr-0 pl-0">
                    <div className="col-12 d-flex justify-content-end">{this.props.history}</div>
                    <div className="col-12 d-flex justify-content-end">{Intl.NumberFormat("de-DE").format(this.props.result)}</div>
                </div>
            )
        }
    }
}